$(document).ready(function(){
    $('#selectEmail').select2({
        dropdownParent: $('#labelModal'),
        placeholder: "Enter email or username",
    });

    $('#selectHolidays').select2({
        dropdownParent: $('#labelModal'),
        placeholder: "Select Holidays",
    });
    
    $('#selectSendTo').select2({
        dropdownParent: $('#labelModal'),
        placeholder: "Select Holidays",
    });

    $('#selectBA').select2({
        dropdownParent: $('#labelModal'),
        placeholder: "Select Holidays",
    });

    $('#selectDue').select2({
        dropdownParent: $('#labelModal'),
        placeholder: "Select Holidays",
    });

    $('#selectResponsible').select2({
        placeholder: "Select Holidays",
    });

    $('#selectDivision').select2({
        dropdownParent: $('#typeModal'),
        placeholder: "Select Division",
    });

    $('#selectDepartment').select2({
        dropdownParent: $('#typeModal'),
        placeholder: "Select Department",
    });

    $('#selectTrainingDivision').select2({
        placeholder: "Select Division",
    });

    $('#selectTrainingDepartment').select2({
        placeholder: "Select Department",
    });

    $('#selectDivision2').select2({
        dropdownParent: $('#jobModal'),
        placeholder: "Select Division",
    });

    $('#selectDepartment2').select2({
        dropdownParent: $('#jobModal'),
        placeholder: "Select Department",
    });

    $('#selectDivisionPolicy').select2({
        dropdownParent: $('#policyModal'),
        placeholder: "Select Division",
    });

    $('#selectDepartmentPolicy').select2({
        dropdownParent: $('#policyModal'),
        placeholder: "Select Department",
    });

    $('#selectRole').select2({
        dropdownParent: $('#divisionModal'),
        placeholder: "Select Role",
    });

    $('#selectType').select2({
        dropdownParent: $('#divisionModal'),
        placeholder: "Select Type",
    });

    var swiper = new Swiper(".onBoarding", {
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
    });
    
});

/*********************/
/***** PASSWORD ******/
/*********************/
function password_show_hide() {
    var x = document.getElementById("InputPassword");
    var show_eye = document.getElementById("show_eye");
    var hide_eye = document.getElementById("hide_eye");
    hide_eye.classList.remove("d-none");
    if (x.type === "password") {
        x.type = "text";
        show_eye.style.display = "none";
        hide_eye.style.display = "block";
    } else {
        x.type = "password";
        show_eye.style.display = "block";
        hide_eye.style.display = "none";
    }
}

function password_show_hide2() {
    var x = document.getElementById("ConfirmPassword");
    var show_eye = document.getElementById("show_eye2");
    var hide_eye = document.getElementById("hide_eye2");
    hide_eye.classList.remove("d-none");
    if (x.type === "password") {
        x.type = "text";
        show_eye.style.display = "none";
        hide_eye.style.display = "block";
    } else {
        x.type = "password";
        show_eye.style.display = "block";
        hide_eye.style.display = "none";
    }
}

/*********************/
/***** DRAW PATH *****/
/*********************/

//helper functions, it turned out chrome doesn't support Math.sgn() 
function signum(x) {
    return (x < 0) ? -1 : 1;
}
function absolute(x) {
    return (x < 0) ? -x : x;
}

function drawPath(svg, path, startX, startY, endX, endY) {
    // get the path's stroke width (if one wanted to be  really precize, one could use half the stroke size)
    var stroke =  parseFloat(path.attr("stroke-width"));
    // check if the svg is big enough to draw the path, if not, set heigh/width
    if (svg.attr("height") <  endY)                 svg.attr("height", endY);
    if (svg.attr("width" ) < (startX + stroke) )    svg.attr("width", (startX + stroke));
    if (svg.attr("width" ) < (endX   + stroke) )    svg.attr("width", (endX   + stroke));
    
    var deltaX = (endX - startX) * 0.15;
    var deltaY = (endY - startY) * 0.15;
    // for further calculations which ever is the shortest distance
    var delta  =  deltaY < absolute(deltaX) ? deltaY : absolute(deltaX);

    // set sweep-flag (counter/clock-wise)
    // if start element is closer to the left edge,
    // draw the first arc counter-clockwise, and the second one clock-wise
    var arc1 = 0; var arc2 = 1;
    if (startX > endX) {
        arc1 = 1;
        arc2 = 0;
    }
    // draw tha pipe-like path
    // 1. move a bit down, 2. arch,  3. move a bit to the right, 4.arch, 5. move down to the end 
    path.attr("d",  "M"  + startX + " " + startY +
                    " V" + (startY + delta) +
                    " A" + delta + " " +  delta + " 0 0 " + arc1 + " " + (startX + delta*signum(deltaX)) + " " + (startY + 2*delta) +
                    " H" + (endX - delta*signum(deltaX)) + 
                    " A" + delta + " " +  delta + " 0 0 " + arc2 + " " + endX + " " + (startY + 3*delta) +
                    " V" + endY );
}

function connectElements(svg, path, startElem, endElem) {
    var svgContainer= $("#svgContainer");

    // if first element is lower than the second, swap!
    if(startElem.offset().top > endElem.offset().top){
        var temp = startElem;
        startElem = endElem;
        endElem = temp;
    }

    // get (top, left) corner coordinates of the svg container   
    var svgTop  = svgContainer.offset().top;
    var svgLeft = svgContainer.offset().left;

    // get (top, left) coordinates for the two elements
    var startCoord = startElem.offset();
    var endCoord   = endElem.offset();

    // calculate path's start (x,y)  coords
    // we want the x coordinate to visually result in the element's mid point
    var startX = startCoord.left + 0.5*startElem.outerWidth() - svgLeft;    // x = left offset + 0.5*width - svg's left offset
    var startY = startCoord.top  + startElem.outerHeight() - svgTop;        // y = top offset + height - svg's top offset

        // calculate path's end (x,y) coords
    var endX = endCoord.left + 0.5*endElem.outerWidth() - svgLeft;
    var endY = endCoord.top  - svgTop;

    // call function for drawing the path
    drawPath(svg, path, startX, startY, endX, endY);

}



function connectAll() {
    // connect all the paths you want!
    connectElements($("#svg1"), $("#path1"), $("#board1"),   $("#board6"));
    connectElements($("#svg1"), $("#path2"), $("#board3"),    $("#board7"));
    connectElements($("#svg1"), $("#path3"), $("#board4"),   $("#board5")  );

}

$(document).ready(function() {
    // reset svg each time 
    $("#svg1").attr("height", "0");
    $("#svg1").attr("width", "0");
    connectAll();
});

$(window).resize(function () {
    // reset svg each time 
    $("#svg1").attr("height", "0");
    $("#svg1").attr("width", "0");
    connectAll();
});

document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
  
    var calendar = new FullCalendar.Calendar(calendarEl, {
      timeZone: 'UTC',
      initialView: 'timeGridWeek',
      headerToolbar: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      events: '/api/demo-feeds/events.json'
    });
  
    calendar.render();
  });

  const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
  const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))

function showInput() {
    document.getElementById('inputDiv').style.display = "block";
    document.getElementById('btnInput').style.display = "none";
}

function editInput() {
    document.getElementById('inputDiv').style.display = "block";
    document.getElementById('resultDiv').style.display = "none";
}

function showTime() {
    document.getElementById('formTime').style.display = "flex";
    document.getElementById('labelTime').style.display = "none";
}

var inputDiv = document.getElementById("inputDiv");
inputDiv.addEventListener("keydown", function(e) {
  if (e.keyCode === 13) {
    e.preventDefault();

    validate(e);
  }
});

function validate(e) {
    document.getElementById("resultDiv").style.display = "block";
    document.getElementById('inputDiv').style.display = "none";
}

function dosomething(){
    sweetAlert(
        'Payroll Submitted',
        '8 of 20 employees already get payslip. You can retry to send it again by clicking the button.',
        'success'
    );
};

function savesomething(){
    sweetAlert(
        'Are You Sure?',
        'If you save & continue by clicking the button, you have to input manually the error and warning data',
        'warning',
    );
};

function savepin(){
    sweetAlert(
        'Verification Success',
        'Your email has been changed.',
        'success'
    );
};

function log (message) {
    document.querySelector('#loggerTxt').value = message
}

window.onload = function () {
    document.querySelector('#datepicker1').addEventListener('datechanged', function(e) {
        console.log('New date', e.data, this.value)
    })

    duDatepicker('#datepicker1', {
        format: 'd mmmm yyyy', range: true, clearBtn: true,
        // disabledDays: ['Sat', 'Sun'],
        events: {
            dateChanged: function (data) {
                log('From: ' + data.dateFrom + '\nTo: ' + data.dateTo)
            },
            onRangeFormat: function (from, to) {
                var fromFormat = 'd mmmm yyyy', toFormat = 'd mmmm yyyy';

                if (from.getMonth() === to.getMonth() && from.getFullYear() === to.getFullYear()) {
                    fromFormat = 'd mmmm'
                    toFormat = 'd mmmm yyyy'
                } else if (from.getFullYear() === to.getFullYear()) {
                    fromFormat = 'd mmmm'
                    toFormat = 'd mmmm yyyy'
                }

                return from.getTime() === to.getTime() ?
                    this.formatDate(from, 'd mmmm yyyy') :
                    [this.formatDate(from, fromFormat), this.formatDate(to, toFormat)].join('-');
            }
        }
    })
}

function showShort() {
    document.getElementById('shortQuestion').style.display = "block";
    document.getElementById('emptyQuestion').style.display = "none";
    document.getElementById('paragraphQuestion').style.display = "none";
    document.getElementById('scaleQuestion').style.display = "none";
    document.getElementById('choiceQuestion').style.display = "none";
}
function showParagraph() {
    document.getElementById('shortQuestion').style.display = "none";
    document.getElementById('emptyQuestion').style.display = "none";
    document.getElementById('paragraphQuestion').style.display = "block";
    document.getElementById('scaleQuestion').style.display = "none";
    document.getElementById('choiceQuestion').style.display = "none";
}
function showScale() {
    document.getElementById('shortQuestion').style.display = "none";
    document.getElementById('emptyQuestion').style.display = "none";
    document.getElementById('paragraphQuestion').style.display = "none";
    document.getElementById('scaleQuestion').style.display = "block";
    document.getElementById('choiceQuestion').style.display = "none";
}
function showChoice() {
    document.getElementById('shortQuestion').style.display = "none";
    document.getElementById('emptyQuestion').style.display = "none";
    document.getElementById('paragraphQuestion').style.display = "none";
    document.getElementById('scaleQuestion').style.display = "none";
    document.getElementById('choiceQuestion').style.display = "block";
}
function changeGridView() {
    $('#tableView').removeClass('listView');
    $('#tableView').addClass('gridView');
    $('#btnGrid').addClass('active');
    $('#btnList').removeClass('active');
}
function changeListView() {
    $('#tableView').removeClass('gridView');
    $('#tableView').addClass('listView');
    $('#btnList').addClass('active');
    $('#btnGrid').removeClass('active');
}

function clickEmployees(){
    window.open('payroll-create.html','_self');
}